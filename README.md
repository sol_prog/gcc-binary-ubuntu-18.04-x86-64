# GCC 9.1.0 binary for Ubuntu 18.04 x86-64#

This is my build version of GCC 9.1.0 for Ubuntu 18.04 64 bits. 
If you prefer to compile it yourself check [https://solarianprogrammer.com/2016/10/07/building-gcc-ubuntu-linux/](https://solarianprogrammer.com/2016/10/07/building-gcc-ubuntu-linux/)
If you want to install the provided binary check the above article, you can skip the build part.
